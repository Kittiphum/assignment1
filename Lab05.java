
public class Lab05 {
	
	public static void GetGrade(int score) {
		String Grade = "" ;
		switch (score) {
		case 80:
			Grade = "A";
			break;
		case 70:
			Grade = "B";
			break;
		case 60:
			Grade = "C";
			break;
		case 50:
			Grade = "D";
			break;
		case 40:
			Grade = "F";
			break;
		default:
			Grade = "E";
			break;
		}
		System.out.print("You grade : " + Grade);
	}
	
	public static void main(String[] args) {
		
		int score = 41;
		GetGrade(score);
		
	}
}
