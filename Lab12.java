
public class Lab12 {
	public static void main(String[] args) {
		String String1 = "You and Me";
		String String2 = " you and me ";
		
		System.out.println("㈤头砧 1");
		if(String1 == String2 || String1.equals(String2)) {
			System.out.println("String1 Equals String2");
		}else {
			System.out.println("String1 Not Equals String2");
		}
		
		System.out.println("\n\n㈤头砧 2");
		String search = "You";
		boolean ResultSearch1 = String1.contains(search);
		boolean ResultSearch2 = String2.contains(search);
	
		if(ResultSearch1 == true) {
			System.out.println("String1 has word : " + search);
		}
		if(ResultSearch1 == false) {
			System.out.println("String1 has not word : " + search);
		}
		if(ResultSearch2 == true) {
			System.out.println("String2 has word : " + search);
		}
		if(ResultSearch2 == false) {
			System.out.println("String2 has not word : " + search);
		}
		
		System.out.println("\n\n㈤头砧 3");
		System.out.println("String1 has Length is : " + String1.length());
		System.out.println("String2 has Length is : " + String2.length());
		
		System.out.println("\n\n㈤头砧 4");
		System.out.println("Cut String1 1-4 : " + String1.substring(4,String1.length()));
		System.out.println("Cut String2 1-4 : " + String2.substring(4,String2.length()));
		
		System.out.println("\n\n㈤头砧 5");
		System.out.println("String1 trim : " + String1.trim());
		System.out.println("String2 trim : " + String2.trim());
		
		System.out.println("\n\n㈤头砧 6");
		System.out.println("String1 Upper : " + String1.toUpperCase());
		System.out.println("String2 Upper : " + String2.toUpperCase());
		
		System.out.println("\n\n㈤头砧 7");
		System.out.println("String2 Upper : " + String2.toUpperCase().trim());
	}
}
