
public class Lab01 {
	 public static void main(String[] args) {
		 int Int = 1 ;
		 boolean Boolean = false;
		 String string = "String";
		 System.out.println("Single Line");
		 System.out.print("This is a Int "+ Int + " This is a Boolean " + Boolean + " This is a String " + string);
		 System.out.println("\nMultiple Line");
		 System.out.print("This is a Int "+ Int + "\nThis is a Boolean " + Boolean + "\nThis is a String " + string);
	 }
}
